; Commerce Deploy Promotions makefile

api = 2
core = 7.x

; Contribs
projects[commerce_coupon][download][type] = git
projects[commerce_coupon][download][branch] = 7.x-2.x
projects[commerce_coupon][download][revision] = 8d70886
projects[commerce_coupon][subdir] = contrib

projects[commerce_discount][download][type] = git
projects[commerce_discount][download][branch] = 7.x-1.x
projects[commerce_discount][download][revision] = 3f8fd72
projects[commerce_discount][patch][2276335] = "https://www.drupal.org/files/issues/commerce_discount-2276335-14.patch"
projects[commerce_discount][subdir] = contrib

projects[commerce_discount_extra][download][type] = git
projects[commerce_discount_extra][download][branch] = 7.x-1.x
projects[commerce_discount_extra][download][revision] = ffaa613
projects[commerce_discount_extra][subdir] = "contrib"

projects[commerce_discount_product_category][version] = 1.x
projects[commerce_discount_product_category][download][type] = git
projects[commerce_discount_product_category][download][branch] = 7.x-1.x
projects[commerce_discount_product_category][download][revision] = d6073f4
projects[commerce_discount_product_category][subdir] = contrib
projects[commerce_discount_product_category][patch][2285275] = https://www.drupal.org/files/issues/add_product_category-2285275-4.patch

projects[inline_conditions][download][type] = git
projects[inline_conditions][download][branch] = 7.x-1.x
projects[inline_conditions][download][revision] = 2995c43
projects[inline_conditions][subdir] = contrib

projects[commerce_price_savings_formatter][version] = 1.4
projects[commerce_price_savings_formatter][subdir] = contrib
